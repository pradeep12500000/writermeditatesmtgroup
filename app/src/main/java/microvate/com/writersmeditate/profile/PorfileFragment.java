package microvate.com.writersmeditate.profile;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import microvate.com.writersmeditate.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PorfileFragment extends Fragment {

public Context context;
public View  view;

    public PorfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_porfile, container, false);
        return view;
    }

}
