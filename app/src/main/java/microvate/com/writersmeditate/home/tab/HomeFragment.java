package microvate.com.writersmeditate.home.tab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import microvate.com.writersmeditate.login.LoginActivity;
import microvate.com.writersmeditate.music.MusicActivity;
import microvate.com.writersmeditate.R;
import microvate.com.writersmeditate.home.tab.home.HomeAdapter;
import microvate.com.writersmeditate.home.tab.home.HomeData;
import microvate.com.writersmeditate.home.tab.home.HomeManager;
import microvate.com.writersmeditate.home.tab.home.image.HomeItemClicklisner;
import microvate.com.writersmeditate.home.tab.home.image.ImageModel;
import microvate.com.writersmeditate.utility.NonScrollGridView;
import microvate.com.writersmeditate.utility.SlidingImage_Adapter;


public class HomeFragment extends Fragment implements HomeItemClicklisner {
    public Context context;
    public View view;
    public ArrayList<HomeData> homeDataArrayList;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.Linearlayout_radio)
    LinearLayout LinearlayoutRadio;
    @BindView(R.id.nonScrollGridView)
    NonScrollGridView nonScrollGridView;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.imageView_profile)
    ImageView imageViewProfile;
    private ArrayList<String> sliderlist;
    Unbinder unbinder;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<ImageModel> imageModelArrayList;

    private int[] myImageList = new int[]{
            R.drawable.banner,
            R.drawable.banner,
            R.drawable.banner,
            R.drawable.banner
    };

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        setSlider();
        setdataAdapter();
        scrollView.smoothScrollTo(0, 0);
        return view;
    }

    private void setdataAdapter() {
        homeDataArrayList = new ArrayList<>();

        HomeAdapter homeAdapter = new HomeAdapter(context, new HomeManager().gethomeDataArrayList(), this);
        nonScrollGridView.setAdapter(homeAdapter);
      /*  RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        recyclerViewHome.setAdapter(homeAdapter);
        recyclerViewHome.setLayoutManager(layoutManager);*/
    }

    private void setSlider() {
        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();
        init();
    }

    private void init() {

        if (pager != null) {
            pager.setAdapter(new SlidingImage_Adapter(context, imageModelArrayList));
            indicator.setViewPager(pager);
        }


        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);

        NUM_PAGES = imageModelArrayList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }

                if (pager != null) {
                    pager.setCurrentItem(currentPage++, true);
                }

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    private ArrayList<ImageModel> populateList() {

        ArrayList<ImageModel> list = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void Homeitemclick(int position) {
        Intent intent = new Intent(getActivity(), MusicActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.imageView_profile)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }
}
