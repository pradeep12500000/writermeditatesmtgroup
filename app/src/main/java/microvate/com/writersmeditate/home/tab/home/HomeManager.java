package microvate.com.writersmeditate.home.tab.home;

import java.util.ArrayList;

public class HomeManager {

    public ArrayList<HomeData> gethomeDataArrayList(){

        ArrayList<HomeData> dataArrayList = new ArrayList<>();
        dataArrayList.add(new HomeData("Rumi's Tale","house of lovers","20 sec"));
        dataArrayList.add(new HomeData("Specific Packages","house of lovers","20 sec"));
        dataArrayList.add(new HomeData("Things Yourself","house of lovers","20 sec"));
        dataArrayList.add(new HomeData(" Steps involved","house of lovers","20 sec"));
        dataArrayList.add(new HomeData("Rumi's Tale","house of lovers","20 sec"));
        dataArrayList.add(new HomeData("Steps involved","house of lovers","20 sec"));
        return dataArrayList;
    }

}
