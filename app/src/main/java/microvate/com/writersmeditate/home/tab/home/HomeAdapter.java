package microvate.com.writersmeditate.home.tab.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import microvate.com.writersmeditate.R;
import microvate.com.writersmeditate.home.tab.home.image.HomeItemClicklisner;

public class HomeAdapter extends BaseAdapter {
    public Context context;
    public ArrayList<HomeData> homeDataArrayList;
    private LayoutInflater layoutInflater;
    public HomeItemClicklisner homeItemClicklisner;

    public HomeAdapter(Context context, ArrayList<HomeData> homeDataArrayList, HomeItemClicklisner homeItemClicklisner) {
        this.context = context;
        this.homeDataArrayList = homeDataArrayList;
        this.homeItemClicklisner = homeItemClicklisner;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    /*  @NonNull
      @Override
      public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
          View view = LayoutInflater.from(context).inflate(R.layout.row_home, parent, false);
          return new ViewHolder(view);
      }

      @Override
      public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
          holder.textViewCategory.setText(homeDataArrayList.get(position).getCategory());
          holder.textViewlovers.setText(homeDataArrayList.get(position).getLovers());
          holder.textViewtime.setText(homeDataArrayList.get(position).getTime());
      }

      @Override
      public int getItemCount() {
          return homeDataArrayList.size();
      }
  */
    @Override
    public int getCount() {
        return homeDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.row_home, parent, false);
        TextView textViewCategory, textViewlovers, textViewtime;
        CardView cardView;

        cardView = convertView.findViewById(R.id.card);
        textViewCategory = convertView.findViewById(R.id.textViewCategory);
        textViewlovers = convertView.findViewById(R.id.textViewlovers);
        textViewtime = convertView.findViewById(R.id.textViewtime);

        textViewCategory.setText(homeDataArrayList.get(position).getCategory());
        textViewlovers.setText(homeDataArrayList.get(position).getLovers());
        textViewtime.setText(homeDataArrayList.get(position).getTime());

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeItemClicklisner.Homeitemclick(position);
            }
        });


        return convertView;
    }

}
