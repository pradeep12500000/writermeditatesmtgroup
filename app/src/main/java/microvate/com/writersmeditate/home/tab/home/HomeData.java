package microvate.com.writersmeditate.home.tab.home;

public class HomeData {
    String Category,lovers,time;


    public HomeData(String category, String lovers, String time) {
        this.Category = category;
        this.lovers = lovers;
        this.time = time;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        this.Category = category;
    }

    public String getLovers() {
        return lovers;
    }

    public void setLovers(String lovers) {
        this.lovers = lovers;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
