package microvate.com.writersmeditate.home;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import microvate.com.writersmeditate.home.tab.HomeFragment;
import microvate.com.writersmeditate.R;


public class HomeActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.relativeLayoutFragmentMainContainer)
    RelativeLayout relativeLayoutFragmentMainContainer;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.drawer_layout)
    RelativeLayout drawerLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        tabLayout.addOnTabSelectedListener(this);
        tabLayout.setSelectedTabIndicatorHeight(0);
        tabLayout.getTabAt(0).select();
        replaceFragmenet(new HomeFragment(), false);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        addFragmentToTab(tab.getPosition());
    }

    private void addFragmentToTab(int position) {
        switch (position) {
            case 0:
                replaceFragmenet(new HomeFragment(), false);
                break;
            case 1:
                Toast.makeText(this, "under development", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "under development", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(this, "under development", Toast.LENGTH_SHORT).show();
                break;
            case 4:
                Toast.makeText(this, "under development", Toast.LENGTH_SHORT).show();
                break;

        }
    }
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void replaceFragmenet(Fragment fragment, boolean isAddToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.relativeLayoutFragmentMainContainer, fragment);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }
}
