package microvate.com.writersmeditate.utility;

import android.app.Application;

import ss.com.bannerslider.Slider;

public class WritersMeditateApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Slider.init(new PicassoImageLoadingService(this));

    }
}
