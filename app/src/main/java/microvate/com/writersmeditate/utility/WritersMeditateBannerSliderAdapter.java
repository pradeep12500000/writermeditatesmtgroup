package microvate.com.writersmeditate.utility;

import java.util.ArrayList;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class WritersMeditateBannerSliderAdapter  extends SliderAdapter {
    ArrayList<String> bannerSliderModelArrayList;

    public WritersMeditateBannerSliderAdapter(ArrayList<String> bannerSliderModelArrayList) {
        this.bannerSliderModelArrayList = bannerSliderModelArrayList;
    }

    @Override
    public int getItemCount() {
        return bannerSliderModelArrayList.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.bindImageSlide(bannerSliderModelArrayList.get(position));

    }

}
