package microvate.com.writersmeditate.music;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import microvate.com.writersmeditate.R;

public class MusicActivity extends AppCompatActivity {
    Timer timer;
    List<String> mediaList = new ArrayList<String>();
    TextView tvCtime, tvLtime, tvTitle;
    ImageView imgPlay, imgStop, imgNext, imgPrevious, imgback, imagepic;
    private MediaPlayer mediaPlayer;
    double totaltime = 0.0;
    private Handler durationHandler = new Handler();
    List<String> mySong;
    double curenttime = 0.0;
    SeekBar seekbar;
    boolean seektouch;
    int SongPos = 0;
    private ArrayList<String> songsNamesList;
/*    private ArrayList<String> imageArrayList;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int song[] = {
                R.raw.one,
                R.raw.two,
        };
      /*  int Imagess[] = {
                R.drawable.banner,
                R.drawable.name,
        };*/
        songsNamesList = new ArrayList<>();
        songsNamesList.add("Songs 1");
        songsNamesList.add("Songs 2");


        try {
            mediaList = new ArrayList<String>();
            mediaList.add(String.valueOf(song[0]));
            mediaList.add(String.valueOf(song[1]));
           /* imageArrayList.add(String.valueOf(Imagess[0]));
            imageArrayList.add(String.valueOf(Imagess[1]));*/
            setContentView(R.layout.activity_music);

            tvTitle = (TextView) findViewById(R.id.mytextview);
            imgPlay = (ImageView) findViewById(R.id.btnPaly);
            imgStop = (ImageView) findViewById(R.id.btnPause);
            imgNext = (ImageView) findViewById(R.id.btnNext);
            seekbar = (SeekBar) findViewById(R.id.seekBar1);
            imgPrevious = (ImageView) findViewById(R.id.btnPrevious);
            imgback = (ImageView) findViewById(R.id.imageViewBack);
            imagepic = (ImageView) findViewById(R.id.imagepic);
            String text = songsNamesList.get(0);
            tvTitle.setText(text);
/*
            String integer = imageArrayList.get(0);
            imagepic.setImageResource(Integer.parseInt(integer));*/
            mediaPlayer = MediaPlayer.create(this,
                    Integer.parseInt(mediaList.get(SongPos)));

            tvCtime = (TextView) findViewById(R.id.tvTime);
            tvLtime = (TextView) findViewById(R.id.tvLastTime);
            timer = new Timer();
            totaltime = mediaPlayer.getDuration();

            imgback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });


            imgPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // TODO Auto-generated method stub
                    try {
                        mediaPlayer.reset();
                        SongPos++;
                        setSong(SongPos);
                        totaltime = mediaPlayer.getDuration();
                        curenttime = mediaPlayer.getCurrentPosition();
                        seekbar.setMax(mediaPlayer.getDuration());
                        PlaySong();

                    } catch (Exception ex) {

                    }
                }

            });

            imgNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    // set next song

                    mediaPlayer.reset();
                    if (SongPos >= mediaList.size() - 1)
                        SongPos = 0;
                    else
                        SongPos++;
                    setSong(SongPos);
                    totaltime = mediaPlayer.getDuration();
                    curenttime = mediaPlayer.getCurrentPosition();
                    seekbar.setMax(mediaPlayer.getDuration());
                    PlaySong();
                }

            });

            imgPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    // set previous song
                    mediaPlayer.reset();
                    if (SongPos == 0)
                        SongPos = mediaList.size() - 1;
                    else
                        SongPos--;
                    setSong(SongPos);
                    totaltime = mediaPlayer.getDuration();
                    curenttime = mediaPlayer.getCurrentPosition();
                    seekbar.setMax(mediaPlayer.getDuration());
                    PlaySong();
                }

            });

            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                    seektouch = false;

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                    seektouch = true;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // TODO Auto-generated method stub
                    // increasing song
                    if (seektouch) {
                        curenttime = progress;
                        mediaPlayer.seekTo((int) curenttime);

                    }

                }

            });

            imgPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (SongPos >= mediaList.size())
                        SongPos = 0;
                    curenttime = mediaPlayer.getCurrentPosition();
                    imgPlay.setVisibility(View.GONE);
                    imgStop.setVisibility(View.VISIBLE);
                    seekbar.setMax(mediaPlayer.getDuration());
                    PlaySong();
                }
            });

            imgStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // TODO Auto-generated method stub
                    imgPlay.setVisibility(View.VISIBLE);
                    imgStop.setVisibility(View.GONE);
                    StopSong();
                }

            });

        } catch (Exception ex) {
        }
    }


/*
    public int getRawResIdByName(String resName) {
        String pkgName = this.getPackageName();
        // Return 0 if not found.
        int resID = this.getResources().getIdentifier(resName, "raw", pkgName);
        return resID;
        }
*/


    @SuppressLint("NewApi")
    public void PlaySong() {
        // start song
        mediaPlayer.start();
        if (mediaPlayer.isPlaying()) {
            imgPlay.setVisibility(View.GONE);
            imgStop.setVisibility(View.VISIBLE);
        }

        curenttime = mediaPlayer.getCurrentPosition();
        seekbar.setProgress((int) curenttime);

        // set total time of song

        tvLtime.setText(String.format(
                "%d:%d ",
                TimeUnit.MILLISECONDS.toMinutes((long) totaltime),
                TimeUnit.MILLISECONDS.toSeconds((long) totaltime)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                        .toMinutes((long) totaltime))));

        durationHandler.postDelayed(updateSeekBarTime, 100);
    }

    public void StopSong() {
        curenttime = mediaPlayer.getCurrentPosition();
        mediaPlayer.pause();
    }

    private Runnable updateSeekBarTime = new Runnable() {
        @SuppressLint("NewApi")
        public void run() {
            curenttime = mediaPlayer.getCurrentPosition();
            seekbar.setProgress((int) curenttime);
            tvCtime.setText(String.format(
                    "%d:%d ",
                    TimeUnit.MILLISECONDS.toMinutes((long) curenttime),
                    TimeUnit.MILLISECONDS.toSeconds((long) curenttime)
                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                            .toMinutes((long) curenttime))));
            durationHandler.postDelayed(this, 100);
        }
    };


    public void setSong(int sing) {
        try {
            if (SongPos >= mediaList.size()) {
                SongPos = 0;

            }
            if (mediaPlayer != null) {
                mediaPlayer.pause();
                mediaPlayer.stop();
                mediaPlayer = null;
            }
            mediaPlayer = MediaPlayer.create(this, Integer.parseInt(mediaList.get(SongPos)));
            //  mediaPlayer.setDataSource(mediaList.get(SongPos).toString());
            //  mediaPlayer.prepare();
            String text = songsNamesList.get(sing);
            tvTitle.setText(text);

        /*    String integer = imageArrayList.get(sing);
            imagepic.setImageResource(Integer.parseInt(integer));*/
            PlaySong();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException ex) {
            Toast.makeText(getApplicationContext(), ex.toString(),
                    Toast.LENGTH_LONG).show();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public void onBackPressed() {
//        ();super.onBackPressed
        // TODO Auto-generated method stub
        imgPlay.setVisibility(View.VISIBLE);
        imgStop.setVisibility(View.GONE);
        StopSong();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // TODO Auto-generated method stub
        imgPlay.setVisibility(View.VISIBLE);
        imgStop.setVisibility(View.GONE);
        StopSong();
    }


}

