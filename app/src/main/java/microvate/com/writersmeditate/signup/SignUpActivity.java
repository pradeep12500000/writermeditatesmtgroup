package microvate.com.writersmeditate.signup;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import microvate.com.writersmeditate.R;
import microvate.com.writersmeditate.login.LoginActivity;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.editTextfirstname)
    EditText editTextfirstname;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTexPassword)
    EditText editTexPassword;
    @BindView(R.id.checkbox_AcceptCondition)
    CheckBox checkboxAcceptCondition;
    @BindView(R.id.button_signup)
    Button buttonSignup;
    @BindView(R.id.textView_Signuphere)
    TextView textViewSignuphere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

    }

    @OnClick({R.id.checkbox_AcceptCondition, R.id.button_signup, R.id.textView_Signuphere})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.checkbox_AcceptCondition:
                break;
            case R.id.button_signup:
                break;
            case R.id.textView_Signuphere:
                startActivity(new Intent(this,LoginActivity.class));
                finish();
                break;
        }
    }
}
