package microvate.com.writersmeditate.enterapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.VideoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import microvate.com.writersmeditate.R;
import microvate.com.writersmeditate.home.HomeActivity;
import microvate.com.writersmeditate.utility.FullScreenVideoView;

public class EnterApp_Activity extends AppCompatActivity {


    @BindView(R.id.videoView)
    FullScreenVideoView videoView;
    @BindView(R.id.button_EnterApp)
    Button buttonEnterApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_app_);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        VideoView view = (VideoView) findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.video;
        view.setVideoURI(Uri.parse(path));
        view.start();
    }

    @OnClick(R.id.button_EnterApp)
    public void onViewClicked() {
        startActivity(new Intent(EnterApp_Activity.this,HomeActivity.class));
        finish();
    }
}

